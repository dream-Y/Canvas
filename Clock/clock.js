const CANVAS_WIDTH = document.body.clientWidth;
const CANVAS_HEIGHT = document.body.clientHeight;
const RADIUS = Math.round(CANVAS_WIDTH * 4 / 5 / 108) - 1;
const MARGIN_TOP = Math.round(CANVAS_HEIGHT / 5);
const MARGIN_LEFT = Math.round(CANVAS_WIDTH / 10);
const HOURS_LATER = 8;

var endTime = new Date();
endTime.setTime(endTime.getTime() + 3600 * HOURS_LATER * 1000);
var curShowTimeSeconds = 0;

var balls = [];
const colors = ['#325B69', '#698570', '#AE5548', '#6D9EA8', '#9CC2B0',
    '#C98769', '#FED675', '#BA78C45', '#DDDC34', '#E45768',
    '#5EC6A8', '#76548F', '#BBBCC66', '#55EE6C', '#FDBCAE',
    '#CC6666', '#666666', '#FCE456', '#333EDC', '#DEA116'
]

window.onload = function() {
    let canvas = document.getElementById('canvas');
    let context = canvas.getContext('2d');

    canvas.width = CANVAS_WIDTH;
    canvas.height = CANVAS_HEIGHT;

    var redraw = setInterval(() => {
        render(context);
        curShowTimeSeconds = getCurShowTimeSeconds();
    }, 50);
}


function getCurShowTimeSeconds() {
    let curTime = new Date();
    let ret = endTime.getTime() - curTime.getTime();
    ret = Math.round(ret / 1000); // Math.round转换整数

    if (ret <= 0) {
        clearInterval(redraw);
    }
    return ret > 0 ? ret : 0;
}

function render(cxt) {
    // 清除图像
    cxt.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);


    let hours = parseInt(curShowTimeSeconds / 3600);
    let minutes = parseInt((curShowTimeSeconds % 3600) / 60);
    let seconds = parseInt(curShowTimeSeconds % 60);

    renderDigit(MARGIN_LEFT, MARGIN_TOP, parseInt(hours / 10), cxt, false);
    renderDigit(MARGIN_LEFT + 15 * (RADIUS + 1), MARGIN_TOP, parseInt(hours % 10), cxt, false);
    renderDigit(MARGIN_LEFT + 30 * (RADIUS + 1), MARGIN_TOP, 10, cxt, false);
    renderDigit(MARGIN_LEFT + 39 * (RADIUS + 1), MARGIN_TOP, parseInt(minutes / 10), cxt, false);
    renderDigit(MARGIN_LEFT + 54 * (RADIUS + 1), MARGIN_TOP, parseInt(minutes % 10), cxt, false);
    renderDigit(MARGIN_LEFT + 69 * (RADIUS + 1), MARGIN_TOP, 10, cxt, false);
    renderDigit(MARGIN_LEFT + 78 * (RADIUS + 1), MARGIN_TOP, parseInt(seconds / 10), cxt, true);
    renderDigit(MARGIN_LEFT + 93 * (RADIUS + 1), MARGIN_TOP, parseInt(seconds % 10), cxt, true);

    let cnt = 0;
    balls.forEach((item, i) => {
        if (item.x - RADIUS > 0 && item.x + RADIUS < CANVAS_WIDTH) {
            balls[cnt++] = item;
        }

        cxt.fillStyle = item.color;
        cxt.beginPath();
        cxt.arc(item.x, item.y, RADIUS, 0, 2 * Math.PI);
        cxt.closePath();
        cxt.fill();

        item.x += item.vx;
        item.y += item.vy;
        item.vy += item.g;

        if (item.y >= CANVAS_HEIGHT - RADIUS) {
            item.y = CANVAS_HEIGHT - RADIUS;
            item.vy = -(item.vy * 0.75);
        }
    });

    while (balls.length > cnt /*Math.min(300, cnt)*/ ) {
        balls.pop();
    }
    console.log(balls.length);

}

function renderDigit(x, y, num, cxt, b) {
    cxt.fillStyle = 'rgb(0,102,153)';

    digit[num].forEach((item, i) => {
        item.forEach((m, n) => {
            if (m === 1) {
                if (b && (curShowTimeSeconds % 60) != (getCurShowTimeSeconds() % 60)) {
                    let ball = {
                        x: x + (n * 2 + 1) * (RADIUS + 1),
                        y: y + (i * 2 + 1) * (RADIUS + 1),
                        g: 1.5 + Math.random(),
                        vx: Math.pow(-1, Math.ceil(Math.random() * 1000)) * 4,
                        vy: -Math.ceil(Math.random() * 5),
                        color: colors[Math.floor(Math.random() * colors.length)]
                    }

                    balls.push(ball)
                }

                cxt.beginPath();
                cxt.arc(x + (n * 2 + 1) * (RADIUS + 1), y + (i * 2 + 1) * (RADIUS + 1), RADIUS, 0, 2 * Math.PI);
                cxt.closePath();
                cxt.fill();
            }
        });
    });
}