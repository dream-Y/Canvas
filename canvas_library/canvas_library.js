// 矩形
function drawRectangle(cxt, x, y, width, height) {
    cxt.beginPath();
    cxt.moveTo(x, y);
    cxt.lineTo(x + width, y);
    cxt.lineTo(x + width, y + height);
    cxt.lineTo(x, y + height);
    cxt.closePath();
}

// 圆形
function drawRound(cxt, x, y, r) {
    cxt.beginPath();
    cxt.arc(x, y, r, 0, 2 * Math.PI);
    cxt.closePath();
}

// 折线
function drawPolyline(cxt, lineArr) {
    cxt.beginPath();
    lineArr.forEach((item, i) => {
        cxt.lineTo(item[0], item[1]);
    });
}

// 多边形
function drawPolygon(cxt, lineArr) {
    drawPolyline(cxt, lineArr);
    cxt.closePath();
};

// 线段
function drawOneline(cxt, x, y, x2, y2) {
    cxt.beginPath();
    cxt.moveTo(x, y);
    cxt.lineTo(x2, y2);
    cxt.closePath();
}